Summary: Send and Receive Files across the Network
Name: warpinator
Version: 1.4.5
Release: 1%{?dist}
License: GPLv2+
URL: https://github.com/linuxmint/warpinator
Source: %url/archive/%{version}/%{name}-%{version}.tar.gz

BuildArch: noarch

BuildRequires: gcc
BuildRequires: meson
BuildRequires: gettext 
BuildRequires: libappstream-glib
BuildRequires: desktop-file-utils

Requires: python3-grpcio
Requires: python3-cryptography
Requires: python3-netifaces
Requires: python3-pynacl
Requires: python3-zeroconf
Requires: python3-packaging
Requires: python3-protobuf
Requires: python3-netaddr
Requires: python3-xapp
Requires: python3-ifaddr
Requires: python3-setproctitle


%description
Warpinator allows you to easily connect multiple computers
on a local area network and share files quickly and securely.

%prep
%setup -q

%build
%meson -Dinclude-firewall-mod=false -Dbundle-zeroconf=false
%meson_build

%install
%meson_install
desktop-file-validate %{buildroot}%{_datadir}/applications/org.x.Warpinator.desktop
desktop-file-validate %{buildroot}%{_sysconfdir}/xdg/autostart/warpinator-autostart.desktop
appstream-util validate --nonet %{buildroot}%{_metainfodir}/org.x.Warpinator.appdata.xml

%find_lang %{name}

%files -f %{name}.lang
%doc README.md
%license  COPYING
%{_bindir}/warpinator
%{_datadir}/icons/hicolor/*/apps/*Warpinator*
%{_datadir}/glib-2.0/schemas/org.x.Warpinator.gschema.xml
%{_datadir}/applications/org.x.Warpinator.desktop
%{_metainfodir}/org.x.Warpinator.appdata.xml
%{_datadir}/warpinator/
%{_libexecdir}/warpinator/*.py
%{_sysconfdir}/xdg/autostart/warpinator-autostart.desktop


%changelog
* Wed Apr 05 2023 Sofia Boldyreva <mooresofia81@gmail.com>
- Removed python3-google-api-core, python3-xapps-overrides from Requires
- Added python3-protobuf, python3-netaddr, python3-ifaddr¶ to Requires
- Added -Dbundle-zeroconf=false option to %meson
- Fixed org.x.Warpinator.desktop, org.x.Warpinator.appdata.xml dirs

* Mon Sep 07 2020 Elagost <me@elagost.com>
- Created spec file

